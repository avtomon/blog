<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 11.04.14
 * Time: 13:50
 */

session_name('avtomon.com');

session_start();

function __autoload ($class)
{
    if (file_exists('classes/class.'.strtolower($class).'.php'))
        require_once 'classes/class.'.strtolower($class).'.php';
    elseif (file_exists('traits/trait.'.strtolower($class).'.php'))
        require_once 'traits/trait.'.strtolower($class).'.php';
}

class RouterException extends Exception { }

    try
    {
        if (!isset($_SESSION['User']['level']))
        {
            unset($_SESSION['User']);
            $_SESSION['User']['level'] = 0;
        }
        if (isset($_REQUEST['page']))
        {
            if ($_REQUEST['page'] == 'login')
            {
                if ($_SESSION['User']['level'] > 0)
                {
                    $_REQUEST['page'] = Registry::defaultUserRedirect($_SESSION['User']['level']);
                }
            }
            $source = Registry::getPageInfo($_REQUEST['page']);
            if ($source['level'] > $_SESSION['User']['level'])
            {
                if ($_SESSION['User']['level'] === 0)
                {
                    header('Location: /login?referer=' . $_REQUEST['page']);
                }
                else
                {
                    header('Location: /error/403');
                }
            }
            $page = Template::create($source['file']);
            if (isset($source['methods']))
            {
                if (!isset($source['methods'][0]))
                {
                    $tmp = $source['methods'];
                    unset($source['methods']);
                    $source['methods'][0] = $tmp;
                }
                if (is_array($source['methods']))
                {
                    foreach ($source['methods'] AS $method)
                    {
                        if (Registry::getMethodAccessLevel($method['class'].'::'.$method['method']) <= $_SESSION['User']['level'])
                        {
                            $response = Service::executeRequest($method);
                            if (is_array($response))
                            {
                                $page->parseResponse($response);
                            }
                            else
                            {
                                if (isset($source['redirect']))
                                {
                                    header("Location: {$source['redirect']}");
                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new RouterException('Неверный формат информации об используемых на странице методах');
                }
            }
            /*else
            {
                echo file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$tpl_name);
            }*/
            /*if (isset($source['sessions']))
            {
                if (is_array($source['sessions']))
                {
                    $page->insertSessionData($source['sessions']);
                }
                else
                {
                    throw new RouterException('Неверный формат информации об используемых на странице переменных сессии');
                }
            }*/

            /*if (fastcgi_finish_request() && isset($source['expire']) && $source['expire'])
            {
                $mc = MShell::create();
                $mc->setHTML(zlib_encode($html, ZLIB_ENCODING_GZIP), $_SERVER['REQUEST_URI'], $source['expire']);
            }*/
            echo $page->getTemplate()->html();
        }
        else
        {
            if (isset($_REQUEST['JSONData']))
            {
                $source = json_decode($_REQUEST['JSONData'], true);
                if (isset($source['setsession']) && isset($source['key']) && isset($source['value']))
                {
                    $_SESSION[$source['key']] = $source['value'];
                    echo json_encode(['setsession' => 'ok'], JSON_UNESCAPED_UNICODE);
                }
                elseif (isset($source['getsession']) && isset($source['key']))
                {
                    echo json_encode(['getsession' => 'ok'], JSON_UNESCAPED_UNICODE);
                }
            }
            else
            {
                if (isset($_FILES) && is_array($_FILES) && count($_FILES))
                {
                    if  ($_FILES[array_keys($_FILES)[0]]['name'])
                    {
                        $files = service::saveFiles($_FILES);
                        $_REQUEST = $_REQUEST + $files;
                    }
                }
                $tmp = $_REQUEST;
                unset ($tmp['submit_data']);
                $_REQUEST = json_decode($_REQUEST['submit_data'], true);
                $_REQUEST['method_params'] = $tmp;
                $source = $_REQUEST;
            }
            if (Registry::getMethodAccessLevel($source['class'].'::'.$source['method']) <= $_SESSION['User']['level'])
            {
                $result = Service::executeRequest($source);
                echo json_encode(['success' => $result], JSON_UNESCAPED_UNICODE);
            }
            else
            {
                if ($_SESSION['User']['level'] === 0)
                {
                    echo json_encode(['redirect' => '/login'], JSON_UNESCAPED_UNICODE);
                }
                else
                {
                    echo json_encode(['error' => 'Недостаточно прав для исполнения метода'], JSON_UNESCAPED_UNICODE);
                }
            }
        }
    }
    catch (Exception $e)
    {
        echo json_encode(['error' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
    }

?>