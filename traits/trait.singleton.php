<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.06.14
 * Time: 12:14
 */

trait Singleton
{
    private static $instance = false;

    public function create ()
    {
        try
        {
            if (!self::$instance)
            {
                $class = __CLASS__;
                self::$instance = new $class();
            }
            return self::$instance;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    private function __construct ()
    {
        try
        {
            if (!self::$db)
                try
                {
                    self::$db = MShell::create();
                }
                catch (Exception $e)
                {
                    throw $e;
                }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

} 