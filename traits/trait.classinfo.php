<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 29.04.14
 * Time: 16:28
 */

trait Classinfo
{
    public function getNativeProperty ($name)
    {
        try
        {
            if (isset($this->$name))
                return $this->$name;
            elseif (isset(self::$$name))
                return self::$$name;
            else
                throw new SuperclassException("Свойства $name нет в классе");
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function setNativeProperty ($name, $value)
    {
        try
        {
            if (isset($this->$name))
                $this->$name = $value;
            elseif (isset(self::$$name))
                self::$$name = $value;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function getClassName ()
    {
        return __CLASS__;
    }
}

?>