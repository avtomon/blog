/**
 * Created by Александр on 17.07.14.
 */

$('.logout').click( function ()
{
    var params = {
        class: 'User',
        method: 'Logout',
        get_instance: false,
        static_method: false
    }
    request(params, true, 'POST', function (data)
    {
        if (data.success == true)
        {
            window.location = '/login';
        }
    });
});

$('body').on('click', '.login', function ()
{
    window.location = '/login?referer=' + window.location.pathname;
});