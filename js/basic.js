/**
 * Created by Александр on 17.07.14.
 */

$('.qsearch').treeListFilter('.root_tree', 200);

$('*').click( function ()
{
    $('.tools_menu').hide();
    $('.group span').removeClass('select');
});

$('body').on("click", "#cancel", function ()
{
    $('#cboxClose').click();
    return false;
});

$('body').on('click', '#submit', function ()
{
    if ($(this).parents('form').find('input[type=file]').length > 0)
        nativeSubmit($(this));
    else
        jsonSubmit($(this));
    return false;
});

$('.toggle').click( function ()
{
    $(this).parents('.parent').eq(0).find('dd').toggleClass('no_display');
    $(this).toggleClass('rotate180');
    return false;
});

function toggleStrings(elem)
{
    $(elem).filter(':not(".clone")').each( function (index, el)
    {
        if ((index % 2) == 0)
        {
            $(el).addClass('odd');
        }
    });
}