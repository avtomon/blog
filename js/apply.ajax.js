/**
 * Created by Александр on 05.05.14.
 */

    function request (params, async, type, callback)
    {
        $.ajax({
            type: type,
            url: "/router.php",
            dataType: 'json',
            data: {
                JSONData: JSON.stringify(params)
            },
            async: async,
            success: function (data)
            {
                if (data.error !== undefined)
                {
                    $.colorbox({html: 'Произошла ошибка ' + data.error});
                }
                else if (data.redirect !== undefined)
                {
                    window.location = data.redirect;
                }
                else if (data.success !== undefined)
                {
                    if (callback !== undefined)
                        callback(data);
                    else
                        $.colorbox({html: 'Данные успешно отправлены'});
                }
                else
                    $.colorbox({html: 'Произошла ошибка'});
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $.colorbox({html: 'Произошла ошибка ' + textStatus});
            }
        });
    }

    function setMultiData(data, parent, is_update)
    {
        if (data.success !== undefined)
            data = data.success;

        if (data !== null && !data.error && typeof(data) === 'object')
        {
            if (data[0])
            {
                var set_length = data.length;
                if (is_update === true)
                {
                    if (set_length !== parent.length)
                        throw 'Неравное количество объектов данных и объектов для измения';
                }
                for (var i = 0; i < set_length; i++)
                {
                    if (is_update === true)
                    {
                        setData(data[i], parent[i]);
                    }
                    else
                    {
                        var element = $(parent).clone(true).appendTo($(parent).parent());
                        setData(data[i], element);
                    }
                }
                if (!is_update)
                {
                    $(parent).addClass('clone');
                }
            }
            else
                setData('body', data);
        }
        else
            parent.addClass('stop');

        return false;
    }

    function setData (data, parent)
    {
        if (data.success)
            data = data.success;

        if (!data.error && typeof(data) === 'object')
        {
            if (data[0] !== undefined)
                data = data[0];

            parent.removeClass('clone');

            var objkeys = Object.keys(data);
            var len = objkeys.length;
            for (var i = 0; i < len; i++)
            {
                var objkey = objkeys[i];
                if (typeof(data[objkey]) === 'object' && data[objkey] !== null)
                {
                    setMultiData(data[objkey], $(parent).find('.' + objkey));
                }
                if ($(parent).hasClass('in_id_' + objkey))
                {
                    $(parent).attr('id', data[objkey]);
                    $(parent).attr('name', data[objkey]);
                }
                if ($(parent).hasClass('in_class_' + objkey))
                {
                    $(parent).addClass(data[objkey]);
                }
                if ($(parent).hasClass('in_val_' + objkey))
                {
                    $(parent).val(data[objkey]);
                }
                if ($(parent).hasClass('in_title_' + objkey))
                {
                    $(parent).attr('title', data[objkey]);
                }
                if ($(parent).hasClass('in_text_' + objkey))
                {
                    $(parent).prepend(data[objkey]);
                }
                if ($(parent).hasClass('in_href_' + objkey))
                {
                    $(parent).attr('href', data[objkey]);
                }
                if ($(parent).hasClass('in_src_' + objkey))
                {
                    $(parent).attr('src', data[objkey]);
                }


                if ($(parent).find('.in_id_' + objkey))
                {
                    $(parent).find('.in_id_' + objkey).attr('id', data[objkey]);
                    $(parent).find('.in_id_' + objkey).attr('name', data[objkey]);
                }
                if ($(parent).find('.in_class_' + objkey))
                {
                    $(parent).find('.in_class_' + objkey).addClass(data[objkey]);
                }
                if ($(parent).find('.in_val_' + objkey))
                {
                    $(parent).find('.in_val_' + objkey).val(data[objkey]);
                }
                if ($(parent).find('.in_title_' + objkey))
                {
                    $(parent).find('.in_title_' + objkey).attr('title', data[objkey]);
                }
                if ($(parent).find('.in_text_' + objkey))
                {
                    $(parent).find('.in_text_' + objkey).text(data[objkey]);
                }
                if ($(parent).find('.in_href_' + objkey))
                {
                    $(parent).find('.in_href_' + objkey).attr('href', data[objkey]);
                }
                if ($(parent).find('.in_src_' + objkey))
                {
                    $(parent).find('.in_src_' + objkey).attr('src', data[objkey]);
                }
            }
        }
        else
            $(parent).addClass('no_data');

        return false;
    }