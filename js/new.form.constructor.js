/**
 * Created by Александр on 05.05.14.
 */

    function FieldConstructor (html)
    {
        var
            fields_container_id = 'fields',
            form_action = '/router.php',
            notation_class = 'notation',
            form_body = 'popup',
            doc = $(html);

        //doc.find('form').validate();

        var addOptions = function (select_data, field)
        {
            if (select_data.length > 0)
            {
                var len = select_data.length;
                var selected = '';

                for (var i = 0; i < len; i++)
                {
                    if (field.selected == select_data[i][field.value])
                        selected = 'selected';
                    if (select_data[i].id === undefined)
                        select_data[i].id = '';

                    doc.
                        find('#'+field.id).
                        append('<option id="' + select_data[i].id + '" value="' + select_data[i][field.value] + '" ' + selected +' >' +
                                 select_data[i][field.text] +
                               '</option>');
                    selected = '';
                }
            }
        }

        var ajaxOptions = function (field)
        {
            $.ajax({
                type: "GET",
                url: form_action,
                dataType: 'json',
                data: {
                    JSONData: JSON.stringify(field.callback)
                },
                async: false,
                success: function (data)
                {
                    if (data.success)
                        addOptions(data.success, field);
                    else if (data.error)
                        $.colorbox({html: 'Произошла ошибка ' + data.error});
                    else
                        $.colorbox({html: 'Произошла ошибка'});
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    $.colorbox({html: 'Произошла ошибка' + textStatus});
                }
            });
        };

        this.getDoc = function ()
        {
            return doc;
        }

        this.addField = function (field)
        {
            var select_data = { };

            if (field.required)
            {
                field.required = 'required';
            }
            else
            {
                field.required = '';
            }

            if (!field.type)
                field.type = 'text';

            if (!field.id)
                field.id = '';

            if (!field.class)
                field.class = '';

            else if (field.rules !== undefined)
            {
                doc.find('#' + field.id).rules('add', field.rules);
            }

            if (!field.placeholder)
                field.placeholder = '';

            if (!field.value)
                field.value = '';

            if (!field.disabled)
                field.disabled = '';

            if (!field.checked)
            field.checked = '';

            if (field.type === 'file' && field.max_file_size !== undefined)
                field.max_file_size = '<input type="hidden" name="MAX_FILE_SIZE" value="' + field.max_file_size + '" />';
            else
                field.max_file_size = '';

            switch (field.type)
            {
                case 'text':
                case 'password':
                case 'file':
                case 'checkbox':
                    doc.
                        find('#' + fields_container_id + ' table').
                        append('<tr>' +
                                  '<td class="th ' + field.required + '">' +
                                     field.label +
                                  '</td>' +
                                  '<td>' +
                                     field.max_file_size +
                                     '<input class="' + field.class + '" type="' + field.type + '" id="' + field.id + '" name="' + field.id + '" placeholder="' + field.placeholder + '" value="' + field.value + '" ' + field.checked + ' ' + field.disabled + '>' +
                                  '</td>' +
                               '</tr>');
                    break;

                case 'hidden':
                    doc.
                        find('#' + fields_container_id + ' form').
                        append('<input class="' + field.class + '" type="hidden" id="' + field.id + '" name="' + field.id + '" value="' + field.value + '">');
                    break;

                case 'filelist':
                    $.ajax({
                        type: "GET",
                        url: '/html/popups/file.html',
                        dataType: 'html',
                        async: false,
                        success: function (data)
                        {
                            doc.
                                find('#' + fields_container_id + ' div#data').
                                append(data);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            $.colorbox({html: 'Произошла ошибка' + textStatus});
                        }
                    });
                    break;

                case 'select':
                    doc.
                        find('#' + fields_container_id + ' table').
                        append('<tr>' +
                            '<td class="th ' + field.required + '">' + field.label + '</td>' +
                            '<td>' +
                            '<select class="' + field.class + '" id="' + field.id + '" name="' + field.id + '" ></select>' +
                            '</td>' +
                            '</tr>');
                    if (field.options)
                        addOptions(field.options, field);
                    else if (field.callback)
                        ajaxOptions(field);
                    break;

                case 'tags':
                    doc.
                        find('#' + fields_container_id + ' table').
                        append('<tr>' +
                            '<td class="th ' + field.required + '">' + field.label + '</td>' +
                            '<td>' +
                            '<select id="' + field.id + '" name="' + field.id + '" multiple class="tags"></select>' +
                            '</td>' +
                            '</tr>');
                    if (field.options)
                        addOptions(field.options, field);
                    else if (field.callback)
                        ajaxOptions(field);
                    break;

                case 'textarea':
                    if (field.height === undefined)
                        field.height = '';
                    else
                        field.height = 'style="height: ' + field.height + ';"';
                    doc.
                        find('#' + fields_container_id + ' table').
                        append('<tr>' +
                                   '<td class="th ' + field.required + '">' +
                                      field.label +
                                   '</td>' +
                                   '<td>' +
                                      '<textarea class="' + field.class + '" id="' + field.id + '" name="' + field.id + '" placeholder="' + field.placeholder + '" ' + field.height + '></textarea>' +
                                   '</td>' +
                                '</tr>');

                    doc.
                        find('#' + fields_container_id + ' table #' + field.id).val(field.value);
                    break;

                case 'submit':
                    doc.
                        find('#' + fields_container_id + ' form').
                        append('<input type="hidden" style="display: none;" id="submit_data" name="submit_data" value=\'' + JSON.stringify(field.params) + '\'">');
                    if (field.button !== undefined)
                    {
                        doc.
                            find('#submit').
                            text(field.button);
                    }
                    break;
            }
        },

        this.setTitle = function (title)
        {
            doc.
                find('.title').
                text(title);
        },

        this.setSubject = function (subject)
        {
            doc.
                find('.grayLine .enterForm').
                html(subject);
        },

        this.setFieldValue = function (field, value)
        {
            doc.find('#' + field).val(value);
        },

        this.getFieldValue = function (field)
        {
            return doc.find('#' + field).val();
        },

        this.setFieldText = function (field, value)
        {
            doc.find('#' + field).text(value);
        }

        this.getFieldText = function (field)
        {
            return doc.find('#' + field).text();
        }

        this.setAction = function (action)
        {
            doc.
                find('form').
                attr('action', 'action');
        },

        this.setValues = function (object)
        {
            var objkeys = Object.keys(object);
            var len = object.length;
            for (var i = 0; i < len; i++)
            {
                doc.
                    find('#'+objkeys[i]).
                    val(object[objkeys[i]]);
            }
        },

        this.setNotation = function (notation)
        {
            doc.
                find('.' + notation_class).
                html(notation);
        },

        this.setFormClass = function (class_name)
        {
            doc.
                find('#' + fields_container_id + ' form').
                addClass(class_name);
        }

        this.createForm = function (object, class_name)
        {
            this.setFormClass(class_name);

            /*if (tabs !== undefined)
            {
                doc.
                    find('#' + fields_container_id + ' textarea').
                    append('')
            }*/
            var objkeys = Object.keys(object);
            var len = object.length;
            for (var i = 0; i < len; i++)
            {
                this.addField(object[objkeys[i]]);
            }
            doc.
                find('#' + fields_container_id + ' textarea').
                text('');
            return this;
        }

        this.getHTML = function ()
        {
            return '<div class="' + form_body + '">' +
                      doc.
                          find('#' + form_body).
                          html() +
                    '</div>';
        }

    }