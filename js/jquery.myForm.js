/**
 * Created by Александр on 17.06.14.
 */

function nativeSubmit (element)
{
    var form_class = element.closest('form').attr('class');
    $('.' + form_class).trigger('beforeSubmit');
    options = {
        dataType:'json',
        iframe: true,
        success: function(response)
        {
            $('.' + form_class).trigger('afterSubmit', response);
        },
        error: function(response)
        {
            $('.' + form_class).trigger('errorSubmit', response);
        }
    };
    element.closest('form').ajaxSubmit(options);
    return false;
}
