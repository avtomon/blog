/**
 * Created by Александр on 16.07.14.
 */

$.applyConfig = {
    url: '/router.php',
    error: function (XMLHttpRequest, textStatus, errorThrown)
    {
        alert('Произошла ошибка ' + textStatus);
    },
    type: 'POST',
    async: true,
    success:
    {
        main: function ()
        {
            $.alert('Запрос выполнен успешно');
        }
    },
    dataObject: 'success',
    noData: function (element)
    {
        $(element).addClass('no_data');
    },
    cloneClass: 'clone'
};

$.alert = function (message)
{
    alert(message);
};

$.request = function (params)
{
    var parent = this;
    if (params.data === undefined || typeof(params.data) !== 'object')
    {
        $.alert('Неверно заданы параметры запроса');
    }
    else
    {
        $.ajax({
            type: params.type || $.applyConfig.type,
            url: params.url || $.applyConfig.url,
            dataType: 'json',
            data: params.data,
            async: params.async || $.applyConfig.async,
            success: function (data)
            {
                var main = params.success.main || $.applyConfig.success.main || $.noop;
                main();

                params.dataObject = params.dataObject || $.applyConfig.dataObject || null;
                if (params.dataObject && data[params.dataObject])
                    data = data[params.dataObject] ;

                if (typeof(data) === 'object')
                {
                    if (params.success.setMultiData)
                    {
                        $.setMultiData(data, params.success.setMultiData);
                    }
                    if (params.success.setData)
                    {
                        $.setData(data, params.success.setMultiData);
                    }
                }
                else
                {
                    var noData = params.noData || $.applyConfig.noData || $.noop;
                    noData(parent);
                }
            },
            error: params.error || $.applyConfig.error || $.noop
        });
    }
};

$.setData = function (data, parent)
{
    if (data[0] !== undefined)
        data = data[0];

    if ($.applyConfig.cloneClass)
        parent.removeClass($.applyConfig.cloneClass);

    var objkeys = Object.keys(data);
    var len = objkeys.length;
    for (var i = 0; i < len; i++)
    {
        var objkey = objkeys[i];
        if (typeof(data[objkey]) === 'object' && data[objkey] !== null)
        {
            $.setMultiData(data[objkey], $(parent).find('.' + objkey));
        }
        if ($(parent).hasClass('in_id_' + objkey))
        {
            $(parent).attr('id', data[objkey]);
            $(parent).attr('name', data[objkey]);
        }
        if ($(parent).hasClass('in_class_' + objkey))
        {
            $(parent).addClass(data[objkey]);
        }
        if ($(parent).hasClass('in_val_' + objkey))
        {
            $(parent).val(data[objkey]);
        }
        if ($(parent).hasClass('in_title_' + objkey))
        {
            $(parent).attr('title', data[objkey]);
        }
        if ($(parent).hasClass('in_text_' + objkey))
        {
            $(parent).prepend(data[objkey]);
        }
        if ($(parent).hasClass('in_href_' + objkey))
        {
            $(parent).attr('href', data[objkey]);
        }
        if ($(parent).hasClass('in_src_' + objkey))
        {
            $(parent).attr('src', data[objkey]);
        }


        if ($(parent).find('.in_id_' + objkey))
        {
            $(parent).find('.in_id_' + objkey).attr('id', data[objkey]);
            $(parent).find('.in_id_' + objkey).attr('name', data[objkey]);
        }
        if ($(parent).find('.in_class_' + objkey))
        {
            $(parent).find('.in_class_' + objkey).addClass(data[objkey]);
        }
        if ($(parent).find('.in_val_' + objkey))
        {
            $(parent).find('.in_val_' + objkey).val(data[objkey]);
        }
        if ($(parent).find('.in_title_' + objkey))
        {
            $(parent).find('.in_title_' + objkey).attr('title', data[objkey]);
        }
        if ($(parent).find('.in_text_' + objkey))
        {
            $(parent).find('.in_text_' + objkey).text(data[objkey]);
        }
        if ($(parent).find('.in_href_' + objkey))
        {
            $(parent).find('.in_href_' + objkey).attr('href', data[objkey]);
        }
        if ($(parent).find('.in_src_' + objkey))
        {
            $(parent).find('.in_src_' + objkey).attr('src', data[objkey]);
        }
    }
    return false;
};

$.setMultiData = function (data, parent)
{
    if (data[0])
    {
        var set_length = data.length;
        for (var i = 0; i < set_length; i++)
        {
            var element = $(parent).clone(true).appendTo($(parent).parent());
            $.setData(data[i], element);
        }
    }
    else
    {
        $.setData(data, 'body');
    }
    return false;
};