/**
 * Created by Александр on 29.05.14.
 */

$.validator.addMethod('mydate', function (value, element)
{
    return !/[0-9]{1,2}[-\.][0-9]{1,2}[-\.][0-9]{4}/.test(value)
}, 'Неверный формат даты');