/**
 * Created by Александр on 17.06.14.
 */

function jsonSubmit (element)
{
    var form_class = element.closest('form').attr('class');
    $('.' + form_class).trigger('beforeSubmit');
    var params = JSON.parse(element.closest('form').find('#submit_data').val());
    params.method_params = element.closest('form').serializeObject();
    delete params.method_params['submit_data'];
    request(params, true, 'POST', function (data)
    {
        if (data.success)
            $('.' + form_class).trigger('afterSubmit', data);
        else if (data.error)
            $.colorbox({html: 'Произошла ошибка ' + data.error});
        else if (data.redirect)
            window.location = data.redirect;
        else
            $.colorbox({html: 'Произошла ошибка'});
    });
}

function nativeSubmit (element)
{
    var form_class = element.closest('form').attr('class');
    $('.' + form_class).trigger('beforeSubmit');
    options = {
        dataType:'json',
        iframe: true,
        success:    function(response)
        {
            if (response.success)
                $('.' + form_class).trigger('afterSubmit', response);
            else if (response.error)
                $.colorbox({html: 'Произошла ошибка ' + response.error});
            else if (response.redirect)
                window.location = response.redirect;
            else
                $.colorbox({html: 'Произошла ошибка'});
        }
    };
    element.closest('form').ajaxSubmit(options);
    return false;
}
