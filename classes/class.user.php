<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.14
 * Time: 10:45
 */

class UserException extends Exception { }

class User
{
    use Classinfo;

    private static $db = false;

    public function logout ()
    {
        unset($_SESSION['User']);
        unset($this);
        return true;
    }

    public function auth (array $data)
    {
        try
        {
            $sql = "SELECT
                        id,
                        login,
                        level,
                        CONCAT (surname, ' ', name, ' ', fathername) AS name
                     FROM
                        users
                     WHERE
                        hash = :hash";
            if (isset($data['hash']))
            {
                $result = self::$db->getValue($sql, $data, 0);
                if ($result)
                {
                    unset($_SESSION['methods']);
                    unset($_SESSION['User']);
                    return $result;
                }
                else
                {
                    return false;
                }
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    private $checkLogin = "SELECT
                           id
                         FROM
                           users
                         WHERE
                           login = :login";

    private $getUserInfo = "SELECT
                              u.login AS login,
                              u.avatar AS avatar,
                              u.regtime AS regtime,
                              u.socials AS socials,
                              u.is_rus AS is_rus,
                              COUNT(c.*) AS comment_count
                            FROM
                              users AS u
                            LEFT JOIN
                              comments AS c
                              ON
                                c.user_id = u.id
                            WHERE
                              id = :id
                            GROUP BY
                              u.login,
                              u.avatar,
                              u.regtime,
                              u.socials,
                              u.is_rus";

    private $getUserList = "SELECT
                              id,
                              login,
                              is_active
                            FROM
                              users";

    private $saveUser = "INSERT INTO
                           users
                           ([fields])
                         VALUES
                           [expression]
                         RETURNING
                           id, [fields]";

    private $updateUser = "UPDATE
                             users
                           SET
                             [expression]
                           WHERE
                             id = :id
                           RETURNING
                             [fields]";

    private $banUser = "UPDATE
                          users
                        SET
                          is_active = false
                        WHERE
                          id = :id
                        RETURNING
                          id";

    private $activateUser = "UPDATE
                              users
                            SET
                              is_active = true
                            WHERE
                              id = :id
                            RETURNING
                              id";
} 