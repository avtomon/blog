<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 11.04.14
 * Time: 15:09
 */

class ServiceException extends Exception { }

class Service
{
    /**
     * Возвращает страницу ошибки
     *
     * @param $code
     * @return string
     */
    public static function getErrorPage ($code)
    {
        if ($result = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/error/' . $code . '.html'))
            return $result;
        else
            header ($code);
    }

    /**
     * Функция загрузки массива файлов на сервер
     *
     * @param array $files - массив файлов, которые прислала форма и мета-информация о них
     * @return mixed|string
     * @throws Exception
     */
    public static function saveFiles(array $files)
    {
        $result = false;
        foreach ($files AS $field => $file)
        {
            $file_path = Registry::getFilePath($field);
            $upload_path = $_SERVER['DOCUMENT_ROOT'].$file_path . '/';
            if (!$file['error'] && $file['size'])
            {
                if (isset($file_path))
                {
                    $tmp = explode(".", $file['name']);
                    $ext = end(explode(".", $file['name']));
                    $new_name = str_replace($ext, '', $file['name']) . microtime(true);
                    $mime_type = Registry::validateFileMimeType($file['tmp_name']);
                    if (Registry::validateFileExt($ext) && $mime_type && filesize($file['tmp_name']) <= 314572800 && is_uploaded_file($file['tmp_name']))
                    {
                        $new_name = "$new_name.".$ext;
                        $path = $file_path . '/' . $new_name;
                        $f = file_get_contents($file['tmp_name']);
                        if (file_put_contents($upload_path . $new_name, $f))
                        {
                            $result['path'] = $path;
                            $result['name'] = $file['name'];
                            $result['type'] = $mime_type;
                        }
                        else
                            throw new ServiceException("Файл {$file['name']} не был корректно сохранен");
                    }
                    else
                    {
                        unlink($file['tmp_name']);
                        throw new ServiceException('Файл не может быть сохранен на сервере. Проверьте тип, расширение и размер файла (размер не должен превышать 300Мб) и обратитесь к администратору системы');
                    }
                }
                else
                    throw new ServiceException("Не найден путь для файла {$file['name']}");
            }
            else
                throw new  ServiceException("Файл {$file['name']} не был корректно загружен из формы");
        }
        return $result;
    }

    /**
     * Запись в лог исключений
     *
     * @param Exception $e
     * @throws ServiceException
     */
    public static function writeExceptionLog (Exception $e)
    {
        if (!file_put_contents($_SERVER['DOCUMENT_ROOT'] . Registry::getLogPath('exception'), date("d.m.Y H:i:s") . ' ' . $e->getMessage(), FILE_APPEND))
            throw new ServiceException ('Лог исключений не был записан');
    }

    /**
     * Запись в debug-лог
     *
     * @param $message
     * @throws ServiceException
     */
    public static function writeDebugLog ($message)
    {
        if (file_put_contents($_SERVER['DOCUMENT_ROOT'] . Registry::getLogPath('debug'), date("d.m.Y H:i:s") . ' ' . $message, FILE_APPEND))
            throw new ServiceException ('Debug-лог не был записан');
    }

    /**
     * Запись в лог ошибок
     *
     * @param $error
     * @throws ServiceException
     */
    public static function writeErrorLog ($error)
    {
        if (file_put_contents($_SERVER['DOCUMENT_ROOT'] . Registry::getLogPath('error'), date("d.m.Y H:i:s") . ' ' . $error, FILE_APPEND))
            throw new ServiceException ('Лог ошибок не был записан');
    }

    /**
     * Формирование строки плейсхолдеров для SQL-запросов
     *
     * @param array $data
     * @param string $type
     * @return string
     */
    public static function createPrepareFields (array &$data, $type = 'insert')
    {
        $string = '';
        if ($type == 'insert')
        {
            if (isset($data[0]))
            {
                foreach ($data AS $index => $value)
                {
                    $tmp = '';
                    foreach ($value AS $key => $v)
                    {
                        $tmp .= ":$key$index,";
                        $data_tmp[$key . $index] = $v;
                    }
                    $string .= '(' . trim($tmp, ',') . '),';
                }
                $string = trim($string, ',');
                $data = $data_tmp;
                unset($data_tmp);
                unset($tmp);
            }
            else
            {
                foreach ($data AS $key => $value)
                {
                    $string .= ":$key,";
                }
                $string = '(' . trim($string, ',') . ')';
            }
        }
        else if ($type == 'inline')
        {
            if (isset($data[0]))
            {
                foreach ($data AS $index => $value)
                {
                    $tmp = '';
                    foreach ($value AS $key => $v)
                    {
                        $tmp .= "$v,";
                    }
                    $string .= '(' . trim($tmp, ',') . '),';
                }
                $string = trim($string, ',');
                unset($tmp);
            }
            else
            {
                foreach ($data AS $key => $value)
                {
                    $string .= "$value,";
                }
                $string = '(' . trim($string, ',') . ')';
            }
        }
        else
        {
            foreach ($data AS $key => $value)
            {
                if ($key !== 'i')
                $string .= "$key = :$key,";
            }
            $string = trim($string, ',');
        }
        return $string;
    }

    /**
     * Убрать из ответа NULL-значения
     *
     * @param $data
     * @return array
     */
    public static function disableNulls ($data)
    {
        if (is_array($data))
        {
            $offNull = function (&$value)
            {
                if ($value === null)
                    return '';
                else
                    return $value;
            };
            if (isset($data[0]))
                foreach ($data AS $key => $value)
                {
                    $data[$key] = array_map($offNull, $value);
                }
            else
                $data = array_map($offNull, $data);
        }

        return $data;
    }

    /**
     * Формирование списка полей для SQL-запросов
     *
     * @param array $data
     * @return string
     */
    public static function createSelectString (array $data)
    {
        if (isset($data[0]))
            $data = $data[0];

        return implode(',', array_keys($data));
    }

    /**
     * Разбор SQL-шаблона
     *
     * @param $request
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public static function sql ($request, &$data)
    {
        try
        {
            if (stripos($request, '[fields]'))
            {
                $request = str_replace('[fields]', self::createSelectString($data), $request);
            }
            if (stripos($request, '[expression]'))
            {
                if (stripos($request, 'EXCEPT') !== false)
                {
                    $expression = self::createPrepareFields($data, 'inline');
                }
                elseif (stripos($request, 'INSERT') !== false)
                {
                    $expression = self::createPrepareFields($data);
                }
                elseif (stripos($request, 'UPDATE') !== false)
                {
                    $expression = self::createPrepareFields($data, 'update');
                }

                if (isset($expression))
                {
                    $request = str_replace('[expression]', $expression, $request);
                }
            }

            return $request;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public static function addAdditionalParams (array $params)
    {
        $tmp = Registry::getAdditionalParams("{$params['class']}::{$params['method']}");

        if (isset($params['method_params']))
            $params['method_params'] = $params['method_params'] + $tmp;
        elseif ($tmp)
            $params['method_params'] = $tmp;

        return $params;
    }

    /**
     * Исполнение метода
     *
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public static function executeRequest (array $params)
    {
        try
        {
            $expire = Registry::getMethodExpire("{$params['class']}::{$params['method']}");

            $params = self::addAdditionalParams($params);

            if (isset($params['method_params']))
            {
                if (is_array($params['method_params']))
                {
                    if (isset($params['method_params'][0]) && is_array($params['method_params'][0]))
                    {
                        foreach ($params['method_params'] AS $key => $value)
                        {
                            $params['method_params'][$key] = Registry::getAllValidateFields($value, "{$params['class']}::{$params['method']}");
                        }
                    }
                    else
                    {
                        $params['method_params'] = Registry::getAllValidateFields($params['method_params'], "{$params['class']}::{$params['method']}");
                    }
                }
                else
                {
                    throw new ServiceException('Формат параметров метода неверен');
                }
            }

            $refclass = new ReflectionClass($params['class']);
            if (!$params['static_method'])
            {
                if (isset($params['get_instance']) && $params['get_instance'])
                {
                    $create = $refclass->getMethod('create');
                    if (isset($params['class_params']))
                        $object = $create->invoke(null, $params['class_params']);
                    else
                        $object = $create->invoke(null);
                }
                else
                    if (isset($params['class_params']))
                        $object = $refclass->newInstance($params['class_params']);
                    else
                        $object = $refclass->newInstance();

                if (isset($params['method_params']) && is_array($params['method_params']))
                {
                    if ($expire)
                    {
                        $result = $object->$params['method']($params['method_params'], $expire);
                    }
                    else
                    {
                        $result = $object->$params['method']($params['method_params']);
                    }
                }
                else
                {
                    if ($expire)
                        $result = $object->$params['method']($expire);
                    else
                        $result = $object->$params['method']();
                }
            }
            else
                if (isset($params['method_params']) && is_array($params['method_params']))
                {
                    if ($expire)
                        $result = $params['class']::$params['method']($params['method_params'], $expire);
                    else
                        $result = $params['class']::$params['method']($params['method_params']);
                }
                else
                {
                    if ($expire)
                        $result = $params['class']::$params['method']($expire);
                    else
                        $result = $params['class']::$params['method']();
                }

            $result = self::disableNulls($result);
            self::setSession($result, "{$params['class']}::{$params['method']}");

            return $result;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Установка значений сессий и куки
     *
     * @param array $data
     * @param $method
     * @throws Exception
     */
    public static function setSession ($data, $method)
    {
        try
        {
            if (is_array($data))
            {
                if (isset($data[0]))
                    $data = $data[0];

                if ($method_data = Registry::getMethodAccessData($method))
                {
                    if (isset($method_data['session']) && $method_data['session'])
                    {
                        $sessions = $method_data['session'];
                        if (isset($sessions['data']) && is_array($sessions['data']))
                        {
                            if ($diff = array_diff($sessions['data'], array_keys($data)))
                            {
                                $message = json_encode($diff, JSON_UNESCAPED_UNICODE);
                                throw new ServiceException("Недостаточно данных для записи в сиссию для метода $method: $message");
                            }
                            if (isset($sessions['parent']))
                            {
                                $parent = $sessions['parent'];
                                foreach ($sessions['data'] AS $value)
                                {
                                    $_SESSION[$parent][$value] = $data[$value];
                                }
                            }
                            else
                            {
                                foreach ($sessions['data'] AS $value)
                                {
                                    $_SESSION[$value] = $data[$value];
                                }
                            }
                        }
                        else
                        {
                            self::writeExceptionLog("Ошибка в формате данных о записываемых сессиях в методе $method");
                        }
                    }
                    if (isset($method_data['cookie']) && $method_data['cookie'])
                    {
                        $cookies = $method_data['cookie'];
                        if (isset($cookies['data']) && is_array($cookies['data']))
                        {
                            if ($diff = array_diff($cookies['data'], array_keys($data)))
                            {
                                $message = json_encode($diff, JSON_UNESCAPED_UNICODE);
                                throw new ServiceException("Недостаточно данных для записи в куки для метода $method: $message");
                            }
                            foreach ($cookies['data'] AS $value)
                            {
                                setcookie($value, $data[$value], time() + 3600);
                            }
                        }
                        else
                        {
                            self::writeExceptionLog("Ошибка в формате данных о записываемых куки в методе $method");
                        }
                    }
                }
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

}

?>