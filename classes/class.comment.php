<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 08.08.14
 * Time: 16:33
 */

class CommentException extends Exception { }

class Comment extends Superclass
{
    use Classinfo;

    private static $db = false;

    private $getPostComments = "SELECT
                                 c.*,
                                 u.login,
                                 u.photo,
                                 f.login
                               FROM
                                 comments AS c
                               LEFT JOIN
                                 users AS u
                                 ON
                                   u.id = c.user_id
                               LEFT JOIN
                                 users AS f
                                 ON
                                   f.id = c.from
                               WHERE
                                 post_id = :post_id
                                   AND
                                 is_active = true
                               ORDER BY
                                 datetime
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getUserComments = "SELECT
                                  c.*,
                                  f.login
                                FROM
                                  comments
                                LEFT JOIN
                                  users AS f
                                  ON
                                    f.id = c.from
                                WHERE
                                  user_id = :user_id
                                    AND
                                  is_active = true
                                ORDER BY
                                  datetime
                                LIMIT
                                  :limit
                                OFFSET
                                  :offset";

    private $newComment =  "INSERT INTO
                             comments
                             ([fields])
                            VALUES
                              [expression]
                            RETURNING
                              id, [fields]";

    private $updatePost = "UPDATE
                           comments
                         SET
                           [expression]
                         WHERE
                           id = :id
                         RETURNING
                            [fields]";

    private $delPost = "DELETE FROM
                           comments
                        WHERE
                          id = :id
                        RETURNING
                          id";

    private $activateComment = "UPDATE
                                  comments
                                SET
                                  is_active = true
                                WHERE
                                  id = :id";

    private $deactivateComment = "UPDATE
                                      comments
                                    SET
                                      is_active = false
                                    WHERE
                                      id = :id";

} 