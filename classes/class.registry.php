<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 11.04.14
 * Time: 14:49
 */

class RegistryException extends Exception { }

class Registry
{
    private static $paths = false;

    private static $fields = false;

    private static $properties = false;

    private static $pages = false;

    private static $methods = false;

    private static $exts = false;

    private static $mimes = false;

    private static $tables = false;

    private static $redirects = false;

    /**
     * Корректный доступ к свойству
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    private static function getGlobal($name)
    {
        try
        {
            if (!is_array(self::$$name) || count(self::$$name) < 1)
            {
                if (!isset($_SESSION[$name]) || !self::$$name = json_decode($_SESSION[$name], true))
                {
                    require_once($_SERVER['DOCUMENT_ROOT'].'/arrays/'.$name.'.php');
                    if (isset($$name))
                    {
                        self::$$name = $$name;
                        $_SESSION[$name] = json_encode($$name, JSON_UNESCAPED_UNICODE);
                    }
                    else
                        throw new RegistryException("Валидирующий массив $name не найден");
                }
            }
            return self::$$name;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Проверка расширения файла на возможность загрузки
     *
     * @param $ext_name
     * @return bool
     * @throws Exception
     */
    public static function validateFileExt ($ext_name)
    {
        try
        {
            $exts = self::getGlobal('exts');
            if (isset($exts[$ext_name]) && $exts[$ext_name])
                return true;
            else
                return false;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Проверка mime-типа файла
     *
     * @param $filename
     * @return bool
     * @throws Exception
     */
    public static function validateFileMimeType ($filename)
    {
        try
        {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime_type = finfo_file($finfo, $filename);
            $mimes = self::getGlobal('mimes');
            if (isset($mimes[$mime_type]) && $mimes[$mime_type])
                return $mime_type;
            else
                return false;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Возвращает путь к директории с заданным типом файлов
     *
     * @param $file_type
     * @return mixed
     * @throws Exception
     */
    public static function getFilePath ($file_type)
    {
        try
        {
            $paths = self::getGlobal('paths');
            if ($paths['files'][$file_type])
                return $paths['files'][$file_type];
            else
                throw new RegistryException ("Для поля $file_type не задан путь сохранения");
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Возвращает путь к указанному лог-файлу
     *
     * @param $type - тип логируемых данных
     * @return mixed
     * @throws Exception
     */
    public static function getLogPath ($type)
    {
        try
        {
            $paths = self::getGlobal('paths');
            if ($paths['logs'][$type])
                return $paths['logs'][$type];
            else
            {
                throw new RegistryException ("Для типа события $type не задан лог");
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Проверить массив входных данных метода
     *
     * @param $params - входные данные
     * @param $method - идентификатор вида Класс::метод, уникально идентифицирующий набор валидирующих данных
     * @return mixed
     * @throws Exception
     */
    public static function getAllValidateFields (array $params, $method)
    {
        try
        {
            $validate = function (array &$params) use ($method)
            {
                $fields = self::getGlobal('fields');
                if (isset($fields[$method]) && $fields[$method])
                {
                    $check = $fields[$method];
                    if ($arr = array_diff_key($check, $params))
                        foreach ($arr AS $key => $value)
                        {
                            if ($value['is_required'])
                                throw new RegistryException ("Неверный набор данных, не хватает поля $key");
                        }
                    foreach ($params AS $key => $value)
                    {
                        if (isset($check[$key]) && $value !== '' && $value !== NULL)
                            self::validateValue($key, $value, $check[$key]);
                        else
                            unset($params[$key]);
                    }
                }
                return $params;
            };

            if (isset($params[0]) && is_array($params[0]))
            {
                foreach ($params AS $key => $seq)
                {
                    $params[$key] = $validate($seq);
                }
            }
            else
            {
                $params = $validate($params);
            }
            return $params;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Проверить заданное значение параметра метода
     *
     * @param $key   - имя параметра
     * @param $value - значение параметра
     * @param $check - набор валидирующих параметров
     * @return bool
     * @throws Exception
     */
    private static function validateValue ($key, $value, array $check)
    {
        try
        {
            if (isset($check['type']))
            {
                if ($check['type'] == 'json')
                {
                    if (!json_decode($value))
                    {
                        throw new RegistryException("Тип данных поля $key не соответствует требуемому типу");
                    }
                }
                else
                {
                    $start_type = gettype($value);
                    if ($start_type != $check['type'])
                    {
                        $tmp = $value;
                        settype($value, $check['type']);
                        settype($value, $start_type);
                        if ($tmp != $value)
                            throw new RegistryException("Тип данных поля $key не соответствует требуемому типу");
                    }
                    if ($check['type'] == 'string' && isset($check['regexp']) && !$regexp_result = preg_match($check['regexp'], $value))
                    {
                        throw new RegistryException("Значение $value не соответствует регулярному выражению {$check['regexp']}");
                    }
                    elseif (($check['type'] == 'integer' || $check['type'] == 'float') && (isset($check['from']) || isset($check['to'])))
                    {
                        if (isset($check['from']) && $value < $check['from'])
                            throw new RegistryException("Значение $value меньше нижнего лимита значения");
                        if (isset($check['to']) && $value > $check['to'])
                            throw new RegistryException("Значение $value больше верхнего лимита значения");
                    }
                }
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }


    /**
     * Проверка свойства
     *
     * @param $method - что хотим сос войством сделать? Записать или достать (set или  get)?
     * @param $key - имя свойства
     * @param array $check - валидирующий массив
     * @param $value - значение массива
     * @return bool
     * @throws Exception
     */
    private static function validateProperty ($method, $key, array $check, $value = '')
    {
        try
        {
            if (isset($check[$method]) && $check[$method])
                if ($method === 'get')
                    return true;
                elseif ($method === 'set')
                    if ($value !== '')
                    {
                        self::validateValue($key, $value, $check);
                        return true;
                    }
                    else
                        throw new RegistryException('Значение свойства не должно равняться пустой строке');
                else
                    throw new RegistryException('Задан неверный идентификатор метода операции со свойством');
            else
                throw new RegistryException("Операция $method запрещена для свойства $key");
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Валидация ассоциативного массива свойств
     *
     * @param $class
     * @param $params
     * @return mixed
     * @throws Exception
     * @throws RegistryException
     */
    public static function getAllValidateProperties ($class, array $params)
    {
        try
        {
            $properties = self::getGlobal('properties');
            $check = $properties[$class];
            foreach ($params AS $key => $value)
            {
                if (isset($check[$key]))
                    if (!self::validateProperty('set', $key, $check[$key], $value))
                        if (isset($check[$key]['default']))
                            $params[$key] = $check[$key]['default'];
                        else
                            unset($params[$key]);
                else
                    unset($params[$key]);
            }
            return $params;
        }
        catch (RegistryException $e)
        {
            throw $e;
        }
    }

    /**
     * Валидация свойства на возвращение
     *
     * @param $class
     * @param $key
     * @throws RegistryException
     * @throws Exception
     * @throws RegisrtryException
     */
    public static function validateGetProperty ($class, $key)
    {
        try
        {
            $properties = self::getGlobal('properties');
            if ($check = $properties[$class][$key])
                self::validateProperty('get', $key, $check);
            else
                throw new RegistryException("Свойства $key нет в классе $class");
        }
        catch (RegisrtryException $e)
        {
            throw $e;
        }
    }

    /**
     * Валиданция свойства на сохранение
     *
     * @param $class
     * @param $key
     * @param $value
     * @return mixed
     * @throws Exception
     * @throws RegistryException
     */
    public static function validateSetProperty ($class, $key, $value)
    {
        try
        {
            $properties = self::getGlobal('properties');
            if ($check = $properties[$class][$key])
            {
                self::validateProperty('set', $key, $value, $check);
            }
            else
                throw new RegistryException("Свойства $key нет в классе $class");
        }
        catch (RegistryException $e)
        {
            throw $e;
        }
    }

    /**
     * Достать информацию о запрашиваемой странице
     *
     * @param $page_name
     * @return mixed
     * @throws Exception
     */
    public static function getPageInfo ($page_name)
    {
        try
        {
            if ($pageinfo = self::getGlobal('pages')[$page_name])
            {
                if (isset($pageinfo['file']))
                    return $pageinfo;
                else
                    throw new RegistryException("Не найдено имя файла шаблона для страницы $page_name");
            }
            else
            {
                header('Location: /error/404.html');
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Достать информацию об уровне доступа метода
     *
     * @param $method
     * @return bool
     * @throws Exception
     */
    public static function getMethodAccessLevel ($method)
    {
        try
        {
            $access_array = self::getGlobal('methods');
            if (isset($access_array[$method]['level']))
            {
                return $access_array[$method]['level'];
            }
            else
            {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Дополнительная информация о методе
     *
     * @param $method
     * @return bool
     * @throws Exception
     */
    public static function getMethodAccessData ($method)
    {
        try
        {
            $access_array = self::getGlobal('methods');
            if (isset($access_array[$method]))
            {
                return $access_array[$method];
            }
            else
            {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public static function getMethodExpire ($method)
    {
        try
        {
            $access_array = self::getGlobal('methods');
            if (isset($access_array[$method]['expire']))
            {
                return $access_array[$method]['expire'];
            }
            else
            {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public static function getAdditionalParams ($method)
    {
        try
        {
            $access_array = self::getGlobal('methods');
            if (isset($access_array[$method]['additional_params']))
            {
                return $access_array[$method]['additional_params'];
            }
            else
            {
                return array();
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public static function defaultUserRedirect ($level)
    {
        $redirects = self::getGlobal('redirects');
        if (isset($redirects[$level]))
        {
            return $redirects[$level];
        }
        else
        {
            throw new RegistryException('Нет подходящего редиректа для данного типа пользователя');
        }
    }

}

?>