<?php
/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 03.04.14
 * Time: 9:32
 */

//namespace CMS;

//use CMS\Registries;

class SuperclassException extends Exception { }

abstract class Superclass
{
    private $properties = false;

    public function __get ($name)
    {
        try
        {
            Registry::validateGetProperty(__CLASS__, $name);
            if (isset($this->properties[$name]))
                return $this->properties[$name];
            else
                return false;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function __set ($name, $value)
    {
        try
        {
            Properties::validateSetProperty(__CLASS__, $name, $value);
            $this->properties[$name] = $value;
        }
        catch (PropertiesException $e)
        {
            throw $e;
        }
    }

    /*public function __construct (array $data = array ())
    {
        try
        {
            if ($data)
            {
                Properties::validateAll($data);
                $this->properties = $data;
            }
            static::setNativeProperty('db', _PDO::create());
        }
        catch (PropertiesException $e)
        {
            throw $e;
        }
    }*/

    public function __construct ()
    {
        try
        {
            static::setNativeProperty('db', MShell::create());
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function __call ($name, array $data = array())
    {
        try
        {
            $property_value = static::getNativeProperty($name);
            $expire = false;
            if ($data)
            {
                if (count($data) > 1)
                {
                    $expire = $data[1];
                    $data = $data[0];
                }
                elseif (is_array($data[0]))
                {
                    $data = $data[0];
                }
                else
                {
                    $expire = $data[0];
                    $data = array();
                }

                if (count($data))
                {
                    if (isset($date[1]))
                    {
                        if (is_array($property_value) && count($data) == count($property_value))
                        {
                            $n = count($data);
                            for ($i = 0; $i < $n; $i++)
                            {
                                $property_value[$i] = Service::sql($property_value[$i], $data[$i]);
                            }
                        }
                        else
                        {
                            throw new SuperclassException('Количество запросов не соответствует количеству массивов параметров');
                        }
                    }
                    else
                    {
                        $property_value = Service::sql($property_value, $data);
                    }
                }
            }
            //print_r($data);
            //print_r($property_value);
            //$result = static::getNativeProperty('db')->query($property_value, $data);
            $result = static::getNativeProperty('db')->getValue($property_value, $data, $expire);

            if ($result)
                return $result;
            /*else
                throw new SuperclassException("Запрос $name отработал некорректно, либо список данных пуст");*/
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

}

?>