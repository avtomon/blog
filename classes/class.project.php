<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 08.08.14
 * Time: 16:52
 */

class ProjectException extends Exception { }

class Project extends Superclass
{
    use Classinfo;

    private static $db = false;

    private $getRusProjectList = "SELECT
                                    id,
                                    title,
                                    demo,
                                    download,
                                    documentation,
                                    description,
                                    where,
                                  FROM
                                    projects
                                  WHERE
                                    is_active = true
                                  ORDER BY
                                    id
                                  LIMIT
                                    :limit
                                  OFFSET
                                    :offset";

    private $getEngProjectList = "SELECT
                                    id,
                                    eng_title,
                                    demo,
                                    download,
                                    eng_documentation,
                                    eng_description,
                                    where,
                                  FROM
                                    projects
                                  WHERE
                                    is_active = true
                                  ORDER BY
                                    id
                                  LIMIT
                                    :limit
                                  OFFSET
                                    :offset";

    private $getEngProject =     "SELECT
                                    id,
                                    eng_title,
                                    demo,
                                    download,
                                    eng_documentation,
                                    eng_description,
                                    where,
                                    eng_pluses
                                  FROM
                                    projects
                                  WHERE
                                    is_active = true
                                      AND
                                    id = :id";

    private $getRusProject =     "SELECT
                                    id,
                                    title,
                                    demo,
                                    download,
                                    documentation,
                                    description,
                                    where,
                                    pluses
                                  FROM
                                    projects
                                  WHERE
                                    is_active = true
                                      AND
                                    id = :id";

    private $newProject =  "INSERT INTO
                             projects
                             ([fields])
                            VALUES
                              [expression]
                            RETURNING
                              id, [fields]";

    private $updateProject = "UPDATE
                               projects
                             SET
                               [expression]
                             WHERE
                               id = :id
                             RETURNING
                                id, [fields]";

    private $delProject = "DELETE FROM
                               projects
                            WHERE
                              id = :id
                            RETURNING
                              id";

    private $activateProject = "UPDATE
                                  projects
                                SET
                                  is_active = true
                                WHERE
                                  id = :id";

    private $deactivateProject = "UPDATE
                                      projects
                                    SET
                                      is_active = false
                                    WHERE
                                      id = :id";
}