<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 08.08.14
 * Time: 14:34
 */

class PostException extends Exception { }

class Post extends Superclass
{
    use Classinfo;

    private static $db = false;

    private $getRusNews = "SELECT
                                 id,
                                 title,
                                 brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Новость'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getEngNews = "SELECT
                                 id,
                                 eng_title,
                                 eng_brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Новость'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getRusArticles = "SELECT
                                 id,
                                 title,
                                 brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Статья'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getEngArticles = "SELECT
                                 id,
                                 eng_title,
                                 eng_brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Статья'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getRusOpinions = "SELECT
                                 id,
                                 title,
                                 brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Мнение'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    private $getEngOpinions = "SELECT
                                 id,
                                 eng_title,
                                 eng_brief,
                                 date
                               FROM
                                 posts
                               WHERE
                                 type = 'Мнение'
                                   AND
                                 is_active = true
                               ORDER BY
                                 id DESC
                               LIMIT
                                 :limit
                               OFFSET
                                 :offset";

    public function addPost (array $data)
    {
        try
        {
            self::$db->beginTransaction();
            $result1 = $this->newPost(Registry::getAllValidateFields($data, 'Post::newPost'));
            if (is_array($result1))
            {
                if (isset($data['tags']))
                {
                    if (is_array($data['tags']))
                    {
                        foreach ($data['tags'] AS $key => $value)
                        {
                            unset($data['tags'][$key]);
                            $data['tags'][$key]['post_id'] = $result1[0]['id'];
                            $data['tags'][$key]['tag_id'] = $value;
                        }
                    }
                    else
                    {
                        $data['tags'] = ['tag_id' => $data['tags'], 'post_id' => $result1[0]['id']];
                    }
                    $result2 = $this->addTagsToPost(Registry::getAllValidateFields($data['tags'], 'Post::addTagsToPost'));
                    if (is_array($result2))
                    {
                        self::$db->commit();
                        return $result1;
                    }
                    else
                    {
                        self::$db->rollBack();
                        throw new PostException ('Не удалось добавить теги к посту');
                    }
                }
                else
                {
                    self::$db->commit();
                    return $result1;
                }
            }
            else
            {
                self::$db->rollBack();
                throw new PostException ('Не удалось сохранить пост');
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function updatePost (array $data)
    {
        try
        {
            self::$db->beginTransaction();
            $result1 = $this->updatePostSQL(Registry::getAllValidateFields($data, 'Post::updatePostSQL'));
            if (is_array($result1))
            {
                $this->clearPostTags(array('post_id' => $data['id']));
                if (isset($data['tags']))
                {
                    if (is_array($data['tags']))
                    {
                        foreach ($data['tags'] AS $key => $value)
                        {
                            unset($data['tags'][$key]);
                            $data['tags'][$key]['post_id'] = $result1[0]['id'];
                            $data['tags'][$key]['tag_id'] = $value;
                        }
                    }
                    else
                    {
                        $data['tags'] = ['tag_id' => $data['tags'], 'post_id' => $result1[0]['id']];
                    }
                    $result2 = $this->addTagsToPost(Registry::getAllValidateFields($data['tags'], 'Post::addTagsToPost'));
                    if (is_array($result2))
                    {
                        self::$db->commit();
                        return $result1;
                    }
                    else
                    {
                        self::$db->rollBack();
                        throw new PostException ('Не удалось добавить теги к посту');
                    }
                }
                else
                {
                    self::$db->commit();
                    return $result1;
                }
            }
            else
            {
                self::$db->rollBack();
                throw new PostException ('Не удалось сохранить пост');
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    private $updatePostSQL = "UPDATE
                             posts
                           SET
                             [expression]
                           WHERE
                             id = :id
                           RETURNING
                             [fields]";

    private $clearPostTags = "DELETE FROM
                                posts_tags
                              WHERE
                                post_id = :post_id";

    private $addTagsToPost = "INSERT INTO
                                posts_tags
                                ([fields])
                              VALUES
                                [expression]
                              RETURNING
                                [fields]";

    private $delTagsFromPost = "DELETE FROM
                                       posts_tags
                                    WHERE
                                      post_id = :post_id
                                        AND
                                      tag_id = :tag_id
                                    RETURNING
                                      id";

    private $newPost = "INSERT INTO
                          posts
                          ([fields])
                        VALUES
                          [expression]
                        RETURNING
                          id, [fields]";

    private $delPost = "DELETE FROM
                           posts
                        WHERE
                          id = :id
                        RETURNING
                          id";

    private $getRusNewsFromTag = "SELECT
                                     p.id,
                                     p.title,
                                     p.brief,
                                     p.date
                                   FROM
                                     posts AS p
                                   LEFT JOIN
                                     posts_tags AS pt
                                     ON
                                       pt.post_id = p.id
                                   WHERE
                                     p.type = 'Новость'
                                       AND
                                     p.is_active = true
                                       AND
                                     pt.tag_id = :tag_id
                                   ORDER BY
                                     p.id DESC
                                   LIMIT
                                     :limit
                                   OFFSET
                                     :offset";

    private $getEngNewsFromTag = "SELECT
                                     p.id,
                                     p.eng_title,
                                     p.eng_brief,
                                     p.date
                                   FROM
                                     posts AS p
                                   LEFT JOIN
                                     posts_tags AS pt
                                     ON
                                       pt.post_id = p.id
                                   WHERE
                                     p.type = 'Новость'
                                       AND
                                     p.is_active = true
                                       AND
                                     pt.tag_id = :tag_id
                                   ORDER BY
                                     p.id DESC
                                   LIMIT
                                     :limit
                                   OFFSET
                                     :offset";

    private $getRusArticlesFromTag = "SELECT
                                         p.id,
                                         p.title,
                                         p.brief,
                                         p.date
                                       FROM
                                         posts AS p
                                       LEFT JOIN
                                         posts_tags AS pt
                                         ON
                                           pt.post_id = p.id
                                       WHERE
                                         p.type = 'Статья'
                                           AND
                                         p.is_active = true
                                           AND
                                         pt.tag_id = :tag_id
                                       ORDER BY
                                         p.id DESC
                                       LIMIT
                                         :limit
                                       OFFSET
                                         :offset";

    private $getEngArticlesFromTag = "SELECT
                                         p.id,
                                         p.eng_title,
                                         p.eng_brief,
                                         p.date
                                       FROM
                                         posts AS p
                                       LEFT JOIN
                                         posts_tags AS pt
                                         ON
                                           pt.post_id = p.id
                                       WHERE
                                         p.type = 'Статья'
                                           AND
                                         p.is_active = true
                                           AND
                                         pt.tag_id = :tag_id
                                       ORDER BY
                                         p.id DESC
                                       LIMIT
                                         :limit
                                       OFFSET
                                         :offset";

    private $getRusOpinionsFromTag = "SELECT
                                         p.id,
                                         p.title,
                                         p.brief,
                                         p.date
                                       FROM
                                         posts AS p
                                       LEFT JOIN
                                         posts_tags AS pt
                                         ON
                                           pt.post_id = p.id
                                       WHERE
                                         p.type = 'Мнение'
                                           AND
                                         p.is_active = true
                                           AND
                                         pt.tag_id = :tag_id
                                       ORDER BY
                                         p.id DESC
                                       LIMIT
                                         :limit
                                       OFFSET
                                         :offset";

    private $getEngOpinionsFromTag = "SELECT
                                         p.id,
                                         p.eng_title,
                                         p.eng_brief,
                                         p.date
                                       FROM
                                         posts AS p
                                       LEFT JOIN
                                         posts_tags AS pt
                                         ON
                                           pt.post_id = p.id
                                       WHERE
                                         p.type = 'Мнение'
                                           AND
                                         p.is_active = true
                                           AND
                                         pt.tag_id = :tag_id
                                       ORDER BY
                                         p.id DESC
                                       LIMIT
                                         :limit
                                       OFFSET
                                         :offset";

    private $getRusPost = "SELECT
                             p.id,
                             title,
                             brief,
                             text,
                             date,
                             json_agg(t) AS tags
                           FROM
                             posts AS p
                           LEFT JOIN
                             posts_tags AS pt
                             ON
                             pt.post_id = p.id
                           LEFT JOIN
                             tags AS t
                             ON
                               pt.tag_id = t.id
                           WHERE
                             p.id = :id";

    private $getEngPost = "SELECT
                             p.id,
                             eng_title,
                             eng_brief,
                             eng_text,
                             date,
                             json_agg(t) AS tags
                           FROM
                             posts AS p
                           LEFT JOIN
                             posts_tags AS pt
                             ON
                             pt.post_id = p.id
                           LEFT JOIN
                             tags AS t
                             ON
                               pt.tag_id = t.id
                           WHERE
                             p.id = :id";

    private $activatePost = "UPDATE
                                  posts
                                SET
                                  is_active = true
                                WHERE
                                  id = :id";

    private $deactivatePost = "UPDATE
                                      posts
                                    SET
                                      is_active = false
                                    WHERE
                                      id = :id";

} 