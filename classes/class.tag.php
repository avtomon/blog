<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 08.08.14
 * Time: 18:01
 */

class TagException extends Exception { }

class Tag
{
    use Classinfo;

    private static $db = false;

    private $getTags = "SELECT
                          *
                        FROM
                          tags
                        WHERE
                          is_active = true";

    private $newTag =  "INSERT INTO
                             tags
                             (tag)
                            VALUES
                              tag = :tag
                            RETURNING
                              id,
                              tag";

    private $updateTag = "UPDATE
                           tag
                         SET
                           tag = :tag
                         WHERE
                           id = :id
                         RETURNING
                            id,
                            tag";

    private $delTag = "DELETE FROM
                           tag
                        WHERE
                          id = :id
                        RETURNING
                          id";

    private $activateTag = "UPDATE
                                  tags
                                SET
                                  is_active = true
                                WHERE
                                  id = :id";

    private $deactivateTag       = "UPDATE
                                      tags
                                    SET
                                      is_active = false
                                    WHERE
                                      id = :id";

} 