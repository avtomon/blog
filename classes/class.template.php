<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.04.14
 * Time: 19:13
 */

class TemplateException extends Exception { }

class Template
{
    private $template = false;

    private $level = false;

    private static $instance = false;

    public static function create ($tpl_name)
    {
        try
        {
            if (!self::$instance)
            {
                $class = __CLASS__;
                self::$instance = new Template($tpl_name);
            }
            return self::$instance;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Инициализация шаблона страницы
     *
     * @param $tpl_name
     */
    private function __construct($tpl_name)
    {
        try
        {
            $this->template = phpQuery::newDocumentFileHTML($_SERVER['DOCUMENT_ROOT'].'/'.$tpl_name);
            /*if ($this->template->find('html')->attr('level') && $this->template->find('html')->attr('level') > $_SESSION['User']['level'])
                //$this->access_denied = Service::getErrorPage(401);
                //throw new TemplateException('Доступ к странице запрещен');
            else
                $this->level = $_SESSION['User']['level'];

            $this->disableLevelUp();*/
            if (isset($_SESSION['User']['login']))
                $this->template->find('.register a')->text($_SESSION['User']['login']);
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function getTemplate ()
    {
        return $this->template;
    }

    public function setMultiData (array $data)
    {
        try
        {
            $parent = $this->getParent($data[1]);
            $clone = $parent->clone();
            $new = '';
            foreach ($data AS $row)
            {
                $new .= $this->setData($row, $clone);
            }
            $parent->parent()->append($new);
            $parent->remove();
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    private function getParent(array $data)
    {
        if (count($data) > 1)
            $tmp_el = array_keys($data)[1];
        else
            $tmp_el = array_keys($data)[0];

        $parent = $this->template->find("*[class$='_$tmp_el']")->parents('.parent');
        if (count($parent))
            return $parent;
        else
            return $this->template->find('body');
    }

    public function insertSessionData (array $keys)
    {
        foreach ($keys AS $key)
        {
            if (isset($_SESSION[$key]))
                $data = $_SESSION[$key];
        }
        $this->setData($data);
    }

    public function setData (array $data, $parent = false)
    {
        try
        {
            if (isset($data[0]))
                $data = $data[0];

            if (!$parent)
            {
                $parent = $this->getParent($data);
            }
            foreach ($data AS $key => $value)
            {
                if ($parent->hasClass('in_id_' . $key))
                {
                    $parent->attr('id', $data[$key]);
                }
                if ($parent->hasClass('in_title_' . $key))
                {
                    $parent->attr('title', $data[$key]);
                }
                if ($parent->hasClass('in_class_' . $key))
                {
                    $parent->addClass($data[$key]);
                }
                if ($parent->hasClass('in_val_' . $key))
                {
                    $parent->val($data[$key]);
                }
                if ($parent->hasClass('in_text_' . $key))
                {
                    $parent->prepend($data[$key]);
                }
                if ($parent->find('.in_id_' . $key))
                {
                    $parent->find('.in_id_' . $key)->attr('id', $data[$key]);
                }
                if ($parent->find('.in_title_' . $key))
                {
                    $parent->find('.in_title_' . $key)->attr('title', $data[$key]);
                }
                if ($parent->find('.in_class_' . $key))
                {
                    $parent->find('.in_class_' . $key)->addClass($data[$key]);
                }
                if ($parent->find('.in_val_' . $key))
                {
                    $parent->find('.in_val_' . $key)->val($data[$key]);
                }
                if ($parent->find('.in_text_' . $key))
                {
                    $parent->find('.in_text_' . $key)->text($data[$key]);
                }
            }
            return $parent;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    public function parseResponse ($response)
    {
        if ($response)
            if (isset($response[1]))
                $this->setMultiData($response);
            else
                $this->setData($response);
    }

    public function disableLevelUp ()
    {
        phpQuery::each($this->template->find('*[level]'), function ($index, $element)
        {
            if ($element->attr('level') > $this->level)
            {
                $element->remove();
            }
            else
            {
                $element->removeAttr('level');
            }
        });
    }

}

?>