<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 28.04.14
 * Time: 11:00
 */

$properties = [
    'class' =>
    [
        'type' => 'string',
        'regexp' => '/\w+$/i',
        'from' => 23,
        'to' => 61,
        'set' => true,
        'get' => false
    ]
];