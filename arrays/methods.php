<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 26.05.14
 * Time: 14:08
 */

$methods = [
    'Test::test' =>
        [
            'level' => 0,
            'session' =>
            [
                'parent' => 'User',
                'data' => array('id', 'name', 'level')
            ],
            'cookie' =>
            [
                'parent' => 'User',
                'data' => array('id', 'name', 'level')
            ],
            'expire' => 120,
            'additional_params' =>
                [
                    'developer_id' => isset($_SESSION['User']['id']) ? $_SESSION['User']['id'] : 0,
                    'last_update_datetime' => time(),
                    'last_update_user_id' => isset($_SESSION['User']['id']) ? $_SESSION['User']['id'] : 0
                ]
        ],

    'Post::getRusNews' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngNews' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getRusArticles' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngArticles' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getRusOpinions' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngOpinions' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::addPost' =>
        [
            'level' => 1,
            'additional_params' =>
                [
                    'date' => time()
                ]
        ],

    'Post::newPost' =>
        [
            'level' => 1
        ],

    'Post::updatePost' =>
        [
            'level' => 1,
            'additional_params' =>
                [
                    'date' => time()
                ]
        ],

    'Post::updatePostSQL' =>
        [
            'level' => 1
        ],

    'Post::clearPostTags' =>
        [
            'level' => 1
        ],

    'Post::addTagsToPost' =>
        [
            'level' => 1
        ],

    'Post::delTagsFromPost' =>
        [
            'level' => 1
        ],

    'Post::delPost' =>
        [
            'level' => 1
        ],

    'Post::getRusNewsFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngNewsFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getRusArticlesFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngArticlesFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getRusOpinionsFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngOpinionsFromTag' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getRusPost' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::getEngPost' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Post::activatePost' =>
        [
            'level' => 1
        ],

    'Post::deactivatePost' =>
        [
            'level' => 1
        ],

    'Comment::getPostComments' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Comment::getUserComments' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Comment::newComment' =>
        [
            'level' => 1,
            'additional_params' =>
                [
                    'user_id' => isset($_SESSION['User']['id']) ? $_SESSION['User']['id'] : 0
                ]
        ],

    'Comment::updateComment' =>
        [
            'level' => 1
        ],

    'Comment::delComment' =>
        [
            'level' => 1
        ],

    'Comment::activateComment' =>
        [
            'level' => 1
        ],

    'Comment::deactivateComment' =>
        [
            'level' => 1
        ],

    'Tag::getTags' =>
        [
            'level' => 1
        ],

    'Tag::newTag' =>
        [
            'level' => 1
        ],

    'Tag::updateTag' =>
        [
            'level' => 1
        ],

    'Tag::delTag' =>
        [
            'level' => 1
        ],

    'Tag::activateTag' =>
        [
            'level' => 1
        ],

    'Project::getRusProjectList' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Project::getEngProjectList' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Project::getEngProject' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Project::getRusProject' =>
        [
            'level' => 0,
            'expire' => 300
        ],

    'Project::newProject' =>
        [
            'level' => 1
        ],

    'Project::updateProject' =>
        [
            'level' => 1
        ],

    'Project::delProject' =>
        [
            'level' => 1
        ],

    'Project::activateProject' =>
        [
            'level' => 1
        ],

    'Project::deactivateProject' =>
        [
            'level' => 1
        ],

    'User::auth' =>
        [
            'level' => 0
        ],

    'User::checkLogin' =>
        [
            'level' => 0
        ],

    'User::getUserInfo' =>
        [
            'level' => 0
        ],

    'User::saveUser' =>
        [
            'level' => 1,
            'additional_params' =>
                [
                    'regtime' => time()
                ]
        ],

    'User::updateUser' =>
        [
            'level' => 1
        ],

    'User::banUser' =>
        [
            'level' => 1
        ],

    'User::activateUser' =>
        [
            'level' => 1
        ]

];

?>