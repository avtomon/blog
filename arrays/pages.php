<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 30.04.14
 * Time: 13:21
 */

$pages = [
    'admin_programs' =>
    [
        'file' => 'html/admin/programs.html',
        'level' => 3,
        'expire' => 120
    ],

    'admin_groups' =>
        [
            'file' => 'html/admin/groups.html',
            'level' => 3,
            'expire' => 120
        ],

    'login' =>
        [
            'file' => 'html/login.html',
            'level' => 0,
            'expire' => 120
        ],

    'current_program' =>
        [
            'file' => $_SESSION['User']['level'] === 2 ? 'html/account/listener/current_program.html' : 'html/account/listener/current_program.html', //'html/account/teacher/current_program.html',
            'level' => 1,
            'expire' => 120,
            'methods' =>
                [
                    'class' => 'Program',
                    'method' => 'getCurrentProgramFromUser',
                    'get_instance' => false,
                    'static_method' => false
                ],
            'redirect' => '/no_current_program'
        ],

    'all_programs' =>
        [
            'file' => 'html/account/listener/all_programs.html',
            'level' => 1,
            'expire' => 120
        ],

    'control' =>
        [
            'file' => 'html/account/listener/control.html',
            'level' => 1,
            'expire' => 120
        ],

    'teacher_control' =>
        [
            'file' => 'html/account/teacher/control.html',
            'level' => 2,
            'expire' => 120
        ],

    'no_current_program' =>
        [
            'file' => 'html/no_current_program.html',
            'level' => 1,
            'expire' => 120
        ],

    'register' =>
        [
            'file' => 'html/register.html',
            'level' => 0,
            'expire' => 120
        ]
];