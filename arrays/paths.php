<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 03.06.14
 * Time: 17:04
 */

$paths = [
    'files' =>
    [
        'material_file' => '/files',
        'photo' => '/photos'
    ],
    'logs' =>
    [
        'debug' => '/logs/debug.log',
        'error' => '/logs/error.log',
        'exception' => '/logs/exception.log'
    ]
];