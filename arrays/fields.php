<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 14.04.14
 * Time: 12:32
 */

$fields = [
    'Test::test' =>
    [
        'new_job_info_speciality_id' =>
        [
            'type' => 'integer',
            'from' => 1,
            'is_required' => true
        ],
        'socworker_id' =>
        [
            'type' => 'integer',
            'from' => 1,
            'is_required' => true
        ]
    ],

    'Post::getRusNews' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::getEngNews' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::getRusArticles' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::getEngArticles' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::getRusOpinions' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::getEngOpinions' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Post::addPost' =>
        [
            'type' =>
                [
                    'type' => 'string',
                    'regexp' => '/(Новость|Статья|Мнение)/',
                    'is_required' => true
                ],
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'date' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'eng_title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'tags' =>
                [
                    'is_required' => false
                ]
        ],

    'Post::newPost' =>
        [
            'type' =>
                [
                    'type' => 'string',
                    'regexp' => '/(Новость|Статья|Мнение)/',
                    'is_required' => true
                ],
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'date' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'eng_title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ]
        ],

    'Post::updatePost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'type' =>
                [
                    'type' => 'string',
                    'regexp' => '/(Новость|Статья|Мнение)/',
                    'is_required' => false
                ],
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'date' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'eng_title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'tags' =>
                [
                    'is_required' => false
                ]
        ],

    'Post::updatePostSQL' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'type' =>
                [
                    'type' => 'string',
                    'regexp' => '/(Новость|Статья|Мнение)/',
                    'is_required' => false
                ],
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'date' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'eng_title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_brief' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ]
        ],

    'Post::clearPostTags' =>
        [
            'post_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::addTagsToPost' =>
        [
            'post_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::delTagsFromPost' =>
        [
            'post_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::delPost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getRusNewsFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getEngNewsFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getRusArticlesFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getEngArticlesFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getRusOpinionsFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getEngOpinionsFromTag' =>
        [
            'tag_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getRusPost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::getEngPost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::activatePost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Post::deactivatePost' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Comment::getPostComments' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Comment::getUserComments' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Comment::newComment' =>
        [
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'datetime' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'user_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'from' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'post_id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Comment::updateComment' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'text' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'datetime' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'from' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Comment::delComment' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Comment::activateComment' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Comment::deactivateComment' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Tag::newTag' =>
        [
            'tag' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ]
        ],

    'Tag::updateTag' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'tag' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ]
        ],

    'Tag::delTag' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Tag::activateTag' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Project::getRusProjectList' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Project::getEngProjectList' =>
        [
            'limit' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'offset' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Project::getEngProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Project::getRusProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ]
        ],

    'Project::newProject' =>
        [
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'demo' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'download' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'documentation' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'pluses' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'description' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'where' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'current_version' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_documentation' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_pluses' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'eng_description' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ]
        ],

    'Project::updateProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'title' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'demo' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'download' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'documentation' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'pluses' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'description' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'where' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'current_version' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_documentation' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'eng_pluses' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'eng_description' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ]
        ],

    'Project::delProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Project::activateProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'Project::deactivateProject' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'User::auth' =>
        [
            'hash' =>
                [
                    'type' => 'string',
                    'regexp' =>'/.{32}/',
                    'is_required' => true
                ]
        ],

    'User::checkLogin' =>
        [
            'login' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ]
        ],

    'User::getUserInfo' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'User::saveUser' =>
        [
            'login' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'hash' =>
                [
                    'type' => 'string',
                    'is_required' => true
                ],
            'avatar' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'regtime' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'socials' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'is_rus' =>
                [
                    'type' => 'string',
                    'regexp' => '/(0|1)/',
                    'is_required' => false
                ]
        ],

    'User::updateUser' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ],
            'login' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'hash' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'avatar' =>
                [
                    'type' => 'string',
                    'is_required' => false
                ],
            'regtime' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => false
                ],
            'socials' =>
                [
                    'type' => 'json',
                    'is_required' => false
                ],
            'is_rus' =>
                [
                    'type' => 'string',
                    'regexp' => '/(0|1)/',
                    'is_required' => false
                ]
        ],

    'User::banUser' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ],

    'User::activateUser' =>
        [
            'id' =>
                [
                    'type' => 'integer',
                    'from' => 1,
                    'is_required' => true
                ]
        ]

];